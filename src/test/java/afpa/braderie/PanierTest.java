package afpa.braderie;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import afpa.braderie.beans.Article;
import afpa.braderie.beans.Panier;
import afpa.braderie.beans.User;
import afpa.braderie.repository.PanierRepository;
import afpa.braderie.repository.UserRepository;
import afpa.braderie.service.ArticleService;
import afpa.braderie.service.PanierService;

@SpringBootTest
public class PanierTest {

	@Autowired
	PanierService servicepanier;

	@Autowired
	PanierRepository panierrepo;

	@Autowired
	UserRepository userrepo;

	@Autowired
	ArticleService articleservice;

	@Test
	public void AjouterAuPanierTest() {

		User user = userrepo.findById(2).get();
		Article article = articleservice.getArticleById(2).get();
		Panier panier = new Panier();
		panier.setArticle(article);
		panier.setUser(user);
		panier.setQuantite(1);
		Panier savedPanier = panierrepo.save(panier);
		assertTrue(savedPanier.getIdpanier() > 0);
	}

}
