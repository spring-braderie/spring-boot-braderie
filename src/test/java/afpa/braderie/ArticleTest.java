package afpa.braderie;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import afpa.braderie.service.ArticleService;

@SpringBootTest
public class ArticleTest {

	@Mock
	@Autowired
	ArticleService articleService;

	/*Test de findAll() article*/
	@Test
	public void testGetAllArticle() {
		articleService.getAllArticle();
	}

	/*Test de findById*/
	@Test
	public void testGetArticleById() {
		articleService.getArticleById(1);
	}
}

