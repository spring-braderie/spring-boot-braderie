package afpa.braderie;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import afpa.braderie.beans.User;
import afpa.braderie.service.UserService;

@SpringBootTest
public class UserTest {

	@Autowired
	UserService userService;

	@Test
	public void testLogin() {

		//Test doit retourner true
		boolean result = userService.isLoginValid("Marleyb", "marleyb123");
		assertTrue(result);

		//Test doit retourner false
		result = userService.isLoginValid("Ma", "marle123");
		assertFalse(result);
	}

	//Test pour récupérer un user
	@Test void getUser(){
		User user = userService.getUser("Marleyb");
		System.out.println(user);
		assertEquals(1, user.getIduser());
	}
}
