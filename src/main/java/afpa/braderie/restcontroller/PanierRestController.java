package afpa.braderie.restcontroller;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import afpa.braderie.beans.Panier;
import afpa.braderie.beans.User;
import afpa.braderie.repository.UserRepository;
import afpa.braderie.service.PanierService;

/**
 * Service Restful du Panier,
 * fourni des API de services permettant d'ajouter, d'afficher, de supprimer, et d'update 
 * la quantité des articles du panier
 * 
 * @author Jordy
 *
 */
@RestController
@RequestMapping("/Panier/")
public class PanierRestController {

	@Autowired
	PanierService panierservice;

	@Autowired
	UserRepository userrepo;
	
	@Autowired
	Logger logger;


	/**Méthode rest pour ajouter un article au panier
	 * @param Idarticle de l'article à ajouter au panier
	 * @param qte de l'article à ajouter
	 * @return une chaine de caractère nous indiquant que la méthode fonctionne
	 */
	@PutMapping("/add/{idarticle}/{qte}")
	public String AjouterAuPanier(@PathVariable("idarticle") Integer Idarticle, @PathVariable("qte") Integer qte) {

		//If user == null retour login

		//recuperer la session lié à l'user dans un objet user

		User user = userrepo.findById(1).get();

		panierservice.AjouterAuPanier(Idarticle, user, qte);

		logger.info("Article ajouté au panier");

		return "ajout réussi";
	}


	/**Méthode rest pour afficher le panier par rapport à un user
	 * @param user du panier
	 * @return le contenu du panier de l'user
	 */
	@GetMapping("/Show/{iduser}")
	public List<Panier> AfficherPanierByUser(User user) {
		logger.info("affichage du panier de" +user.getLogin());
		return panierservice.AfficherPanierbyUser(user);

	}
	

	/**Méthode rest pour supprimer un article d'un panier
	 * @param Idarticle de l'article à supprimer
	 * @param qte la quantité à supprimer
	 * @return nous retourne une chaine de caractere nous indiquant que la méthode fonctionne
	 */
	@DeleteMapping("/delete/{idarticle}/{qte}")
	public String SupprimerDuPanier(@PathVariable("idarticle") Integer Idarticle, @PathVariable("qte") Integer qte) {
		//	public String SupprimerDuPanier(@PathVariable("idarticle") Integer Idarticle, @PathVariable("qte") Integer qte, 
		//			@ModelAttribute(name = "user") User user) {

		//If user == null retour login

		//recuperer la session lié à l'user dans un objet user
		User user = userrepo.findById(1).get();

		panierservice.SupprimerPanier(Idarticle, user);

		logger.info("delete effectué avec succès");

		return "suppression réussi";
	}
	

	/**Méthode rest pour mettre à jour la quantité d'un article du panier
	 * @param Idarticle de l'article à update
	 * @param qte quantité à update
	 * @return un message nous disant que la méthode fonctionne, crud fully opérationnel, youpi !
	 */
	@PostMapping("/update/{idarticle}/{qte}")
	public String update(@PathVariable("idarticle") Integer Idarticle, @PathVariable("qte") Integer qte) {

		User user = userrepo.findById(1).get();

		panierservice.UpdateQuantite(Idarticle, qte, user);
		
		logger.info("update effectué");

		return "update reussi";
	}
}
