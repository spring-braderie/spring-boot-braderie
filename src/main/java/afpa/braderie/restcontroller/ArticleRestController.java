package afpa.braderie.restcontroller;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import afpa.braderie.beans.Article;
import afpa.braderie.service.ArticleService;

/**
 * Service Restful des Articles,
 * fourni des API de services permettant de supprimer, ajouter ou obtenir un ou plusieurs articles
 * 
 * @author Clément F.
 *
 */

@CrossOrigin("*")
@RestController
@RequestMapping("article")
public class ArticleRestController {
	
	/*Injection du service des Articles*/
	@Autowired
	ArticleService articleService;
	
	/*Injection du Logger*/
	@Autowired
	Logger logger;
	
	/**
	 * Méthode GET pour récuperer l'ensemble des articles
	 * de la table t_article
	 * @return une liste d'articles
	 */
	@GetMapping("getall")
	public Iterable<Article> getAllArticle(){
		logger.info("Dans ArticleController.getAllArticle");
		
		return articleService.getAllArticle();
	}
	
	/**
	 * GET pour récupérer un article de la table t_article
	 * via son id
	 * 
	 * @param id de l'article à récupérer
	 * @return l'article récupéré
	 */
	@GetMapping("getbyid/{id}")
	public Optional<Article> getArticle(@PathVariable Integer id) {
		logger.info("Dans ArticleController.getArticle");
		
		return articleService.getArticleById(id);
	}
	
	/**
	 * Méthode DELETE pour supprimer un article de la 
	 * table t_article via son id
	 * @param id de l'article à supprimer
	 */
	@DeleteMapping("deletebyid/{id}")
	public void deleteArticleById(@PathVariable Integer id) {
		logger.info("Dans ArticleController.deleteArticle");
		
		articleService.deleteArticleById(id);
	}	
	
	/**
	 * Méthode DELETE pour supprimer un article de la 
	 * table t_article en passant l'article dans le corps 
	 * de la requête
	 * @param article un pojo Article
	 */
	@DeleteMapping("delete")
	public void deleteArticleById(@RequestBody Article article) {
		logger.info("Dans ArticleController.deleteArticle");
		
		articleService.deleteArticle(article);
	}	

	/**
	 * Méthode PUT pour ajouter un article dans la table
	 * t_article, on passe les éléments de l'article à ajouter
	 * dans le corps de la requête
	 * 
	 * @param article un POJO Article
	 * @return un Article
	 */
	@PutMapping("add")
	public Article addArticle(@RequestBody Article article) {
		logger.info("Dans ArticleController.addArticle");
		
		return articleService.addArticleToCatalogue(article);
	}
}

