package afpa.braderie.service;

import javax.servlet.http.HttpSession;

/**
 * Interface utilisée pour checker les injections url et
 *  si l'user est bien connecté a la session
 * @author Lucas
 *
 */
public interface CheckUserActionInterface {
	
	/**
	 * Méthode verifiant si l'user et dans la session
	 * @param session la session en cours
	 * @return <code>true</code> si l'user est bien connecté dans la session
	 * <code>false</code> s'il n'est pas trouvé
	 */
	public Boolean isValidUser(HttpSession session);

}
