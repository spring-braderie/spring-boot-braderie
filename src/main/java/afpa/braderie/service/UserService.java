package afpa.braderie.service;

import afpa.braderie.beans.User;

/**
 * @author 31010-83-06
 * Interface UserService qui est implémenter par UserServiceImpl
 * Permet de découpler les classes et donc de réduire des dépendances
 * On y retrouve les méthodes du CRUD
 */

public interface UserService {

	boolean isLoginValid(String login, String password);
	
	User getUser(String login);

}
