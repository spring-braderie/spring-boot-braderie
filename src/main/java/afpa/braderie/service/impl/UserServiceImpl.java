package afpa.braderie.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.braderie.beans.User;
import afpa.braderie.repository.UserRepository;
import afpa.braderie.service.UserService;

/**
 * Classe service des users qui implémente l'interface UserService.
 * Injection du Repository de User qui nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * @author 31010-83-06
 *
 */

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userRepository;
	
	/**
	 * méthode vérifiant que le login et le MDP entrées par un utilisateur est dans la BDD
	 * @param login le login entré par un utilisateur 
	 * @param password le mot de passe entré par l'utilisateur
	 * @return un booleen, <code>true</code> si les identifiants sont identiques dans la BDD, <code>false</code> si ils ne correspondent pas 
	 */
	@Override
	public boolean isLoginValid(String login, String password) {
		return userRepository.isValidLogin(login, password);
	}

	/**
	 * méthode permettant de récupérer le User via son login
	 * @param login identifiant à récupérer
	 */
	@Override
	public User getUser(String login) {
		return userRepository.findByLogin(login);
	}
}
