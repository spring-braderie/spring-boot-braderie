package afpa.braderie.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.braderie.beans.Article;
import afpa.braderie.beans.Panier;
import afpa.braderie.beans.User;
import afpa.braderie.repository.ArticleRepository;
import afpa.braderie.repository.PanierRepository;
import afpa.braderie.service.PanierService;

/**
 * Classe service du panier qui implémente l'interface PanierService.
 * Injection du Repository de Panier qui nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * @author Jordy
 *
 */
@Service
@Transactional
public class PanierServiceImpl implements PanierService {

	@Autowired
	ArticleRepository articlerepo;

	@Autowired
	PanierRepository panierrepo;

	@Autowired
	Logger logger;

	/**Méthode pour ajouter un article au panier
	 *
	 */
	@Override
	public Integer AjouterAuPanier(Integer idarticle, User user, Integer quantite) {

		Integer newQte = quantite;
		Article article = articlerepo.findById(idarticle).orElse(null);
		Panier panier = panierrepo.findByArticleAndUser(article, user);

		//Si le panier existe déjà on incrémente la quantité
		if (panier != null) {
			newQte = panier.getQuantite() + quantite;
			panier.setQuantite(newQte);

		//Sinon on crée et remplit un nouveau panier
		} else {

			panier = new Panier();
			panier.setArticle(article);
			panier.setUser(user);
			panier.setQuantite(quantite);

			panierrepo.save(panier);
			logger.info("Article ajouté au panier avec succès");
		}

		return newQte;
	}

	/**Méhtode pour afficher le panier d'un user
	 * @return une liste de panier en lien avec son user
	 */
	@Override
	public List<Panier> AfficherPanierbyUser(User user) {
		logger.info("Le panier contient : ");
		return panierrepo.findByUser(user);
	}

	/**Méthode pour supprimer un article du panier d'un user
	 *
	 */
	@Override
	public void SupprimerPanier(Integer idarticle, User user) {
		panierrepo.deleteByArticleAndUser(idarticle, user.getIduser());
	}

	/**Méthode pour supprimer tout le contenu de la table panier
	 *
	 */
	@Override
	public void SupprimerTout() {
		panierrepo.deleteAll();
	}

	/**Méthode pour update la quantite d'un article directement dans le panier
	 *
	 */
	@Override
	public void UpdateQuantite(Integer idarticle, Integer quantite, User user) {

		panierrepo.updateQuantite(quantite, idarticle, user.getIduser());		
		//		Article article = articlerepo.findById(idarticle).get();
		//		
		//		float soustotal = article.getPrixunitaire() * quantite;
		//		return soustotal;

	}
	
	@Override
	public float getTotal(User user) {
		
		float total = 0;
		List<Panier> list = (List<Panier>) panierrepo.findByUser(user);
		
		for (Panier panier : list) {
			total += panier.getArticle().getPrixunitaire() * panier.getQuantite();
		}
			
		System.out.println(total);
		return total;
		
	}
}
