package afpa.braderie.service.impl;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.braderie.beans.Article;
import afpa.braderie.repository.ArticleRepository;
import afpa.braderie.service.ArticleService;

/**
 * Classe service des articles qui implémente l'interface ArticleService.
 * Injection du Repository de Article qui nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * @author Clément F.
 *
 */
@Service
public class ArticleServiceImpl implements ArticleService {

	/*Injection du Logger*/
	@Autowired
	Logger logger;

	/*Injection du Repository de Article*/
	@Autowired
	ArticleRepository articleRepository;

	/**
	 * Retourne l'article qui correspond à l'id
	 * passé en paramètre
	 * 
	 * @return un Optional d'article, permet d'encapsuler un object null
	 * Evite l'exception nullpointerexception
	 */
	@Override
	public Optional<Article> getArticleById(Integer id) {
		logger.info("Dans ArticleServiceImpl.getArticleById");
		
		logger.info(articleRepository.findById(id));
		return articleRepository.findById(id);
	}

	/**
	 * Ajoute un nouvel Article au catalogue
	 * (nouvelle ligne à la table t_article)
	 * 
	 * @return Un Article qui a été ajouté
	 */
	@Override
	public Article addArticleToCatalogue(Article article) {
		logger.info("Dans ArticleServiceImpl.addArticleToCatalogue");

		logger.info(article);
		return articleRepository.save(article);
	}

	/**
	 * Supprime un article du catalogue via son id
	 * (supprime une ligne de la table t_article)
	 */
	@Override
	public void deleteArticleById(Integer id) {
		logger.info("Dans ArticleServiceImpl.deleteArticleById");

		articleRepository.deleteById(id);
	}

	/**
	 * Supprime un article du catalogue
	 * (supprime une ligne de la table t_article)
	 */
	@Override
	public void deleteArticle(Article article) {	
		logger.info("Dans ArticleServiceImpl.deleteArticle");

		articleRepository.delete(article);
	}

	/**
	 * Retourne la liste des articles qui correspond à la table
	 * t_article
	 * @return un Iterable d'Articles
	 */
	@Override
	public Iterable<Article> getAllArticle() {
		logger.info("Dans ArticleServiceImpl.getAllArticle");

		articleRepository.findAll().forEach(articles -> logger.info(articles.getDescription()+" "+articles.getMarque()+" "+articles.getPrixunitaire()));
		return articleRepository.findAll();
	}
}
