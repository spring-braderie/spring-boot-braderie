package afpa.braderie.service;

import java.util.Optional;

import afpa.braderie.beans.Article;
/**
 * Interface ArticleService qui est implémenter par ArticleServiceImpl
 * Permet de découpler les classes et donc de réduire des dépendances
 * On y retrouve les méthodes du CRUD
 * @author Clément F.
 *
 */
public interface ArticleService {

	public Iterable<Article> getAllArticle();
	
	public Optional<Article> getArticleById(Integer id);
	
	public Article addArticleToCatalogue(Article article);
	
	public void deleteArticleById(Integer id);
	
	public void deleteArticle(Article article);

}
