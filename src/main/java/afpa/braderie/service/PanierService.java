package afpa.braderie.service;

import java.util.List;

import afpa.braderie.beans.Panier;
import afpa.braderie.beans.User;

/**
 * @author jordy
 *
 */

public interface PanierService {
	
	List<Panier> AfficherPanierbyUser(User user);
	
	void SupprimerTout();

	void SupprimerPanier(Integer idarticle, User user);

	Integer AjouterAuPanier(Integer idarticle, User user, Integer quantite);
	
	void UpdateQuantite(Integer idarticle, Integer quantite, User user);
	
	float getTotal(User user);
	
}


