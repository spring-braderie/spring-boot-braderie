package afpa.braderie;

import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * Classe de configuration pour l'internationalisation
 * @author ClémentF.
 *
 */
@Configuration
public class InternationalisationConfig implements WebMvcConfigurer{
	/**
	 * Pour permettre à notre application de déterminer quelle locale est 
	 * utilisée
	 * 
	 * @return LocaleResolver qui determine la locale courante sur la session
	 */
	@Bean
	public LocaleResolver localeResolver() {
	    SessionLocaleResolver slr = new SessionLocaleResolver();
	    slr.setDefaultLocale(Locale.FRENCH);
	    return slr;
	}
	
	/**
	 * On a besoin d'un interceptor pour switcher sur une nouvelle locale basé
	 * sur la valeur du parametre "lang" ajouté à la requête
	 * 
	 * @return LocaleChangeInterceptor
	 */
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
	    LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
	    lci.setParamName("lang");
	    return lci;
	}
	
	/**
	 * Pour prendre effet, le bean LocaleChangeInterceptor doit être
	 * ajouté au registre de l'application
	 * Pour cela on doit implémenter l'interface WebMvcConfigurer et override
	 * la méthode addInterceptors
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
	    registry.addInterceptor(localeChangeInterceptor());
	}
}
