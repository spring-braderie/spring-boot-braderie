package afpa.braderie.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import afpa.braderie.beans.User;

/**
 *  Interface UserRepository qui hérite de l'interface CrudRepository
 * qui permet d'utiliser toutes les méthodes du CRUD
 * Cette interface est injecté dans le service 
 * @author 31010-83-06
 *
 */
@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
	
	@Query(nativeQuery = true, value = "Select isvalidlogin(:login , :password)")
	boolean isValidLogin(@Param("login") String login, @Param("password") String password);
		
	public User findByLogin(String login);	
}
