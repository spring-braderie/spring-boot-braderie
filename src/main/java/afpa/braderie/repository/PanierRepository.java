package afpa.braderie.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import afpa.braderie.beans.Article;
import afpa.braderie.beans.Panier;
import afpa.braderie.beans.User;

/**
 * Interface PanierRepository qui hérite de l'interface CrudRepository
 * qui permet d'utiliser toutes les méthodes du CRUD
 * Cette interface est injecté dans le service 
 * @author jordy
 *
 */
@Repository
public interface PanierRepository extends CrudRepository<Panier, Integer> {
	
	public List<Panier> findByUser(User user);
	
	public Panier findByArticleAndUser(Article article, User user);
	
	@Query("delete from Panier p where p.article.id = ?1 and p.user.iduser = ?2")
	@Modifying
	public void deleteByArticleAndUser(@Param("idarticle") Integer idarticle, @Param("iduser") Integer iduser);

	@Query("UPDATE Panier p SET p.quantite = ?1 WHERE p.article.id = ?2 AND p.user.iduser = ?3")
	@Modifying
	public void updateQuantite(@Param("quantite") Integer quantite, @Param("idarticle") Integer idarticle, @Param("iduser") Integer iduser);
	
}

