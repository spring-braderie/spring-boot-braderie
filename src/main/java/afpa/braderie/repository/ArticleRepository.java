package afpa.braderie.repository;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import afpa.braderie.beans.Article;

/**
 * Interface ArticleRepository qui hérite de l'interface CrudRepository
 * qui permet d'utiliser toutes les méthodes du CRUD
 * Cette interface est injecté le service
 * @author Clément F.
 *
 */
@Repository
public interface ArticleRepository extends CrudRepository<Article, Integer> {
}
