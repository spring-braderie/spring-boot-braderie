package afpa.braderie.controller;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import afpa.braderie.beans.User;
import afpa.braderie.service.CheckUserActionInterface;

/**
 * Implémentation concréte de l'interface {@link  CheckUserActionInterface}
 * @author Lucas
 *
 */
@Component
public class CheckUserAction implements CheckUserActionInterface {

	@Autowired
	Logger logger;
	
	/**
	 * Implémentation concréte de la méthode de l'interface
	 * {@link CheckUserActionInterface#isValidUser(HttpSession)}
	 */
	@Override
	public Boolean isValidUser(HttpSession session) {
		/*
		 * On récupère l'user de la session
		 */
		User userTest = (User) session.getAttribute("user");
		logger.info(userTest);
		/*
		 * On récupère le booleen de validation de la session
		 */
		Boolean isValidToken = (Boolean) session.getAttribute("jeton");
		logger.info(isValidToken);
		
		/*
		 * Si on ne les trouvent pas ou que le jeton est à false on invalide l'action
		 */
		if(userTest==null || isValidToken == null || isValidToken == false)
			return false;
		else
			return true;
	}

}
