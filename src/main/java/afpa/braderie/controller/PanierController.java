package afpa.braderie.controller;

import java.util.List;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;


import afpa.braderie.beans.Panier;
import afpa.braderie.beans.User;
import afpa.braderie.repository.UserRepository;
import afpa.braderie.service.CheckUserActionInterface;
import afpa.braderie.service.PanierService;

/**
 *  Controller de la classe panier qui nous permet d'afficher la vue
 * @author jordy
 *
 */

@Controller
@RequestMapping("/panier")
public class PanierController {

	@Autowired 
	PanierService panierservice;

	@Autowired 
	HttpSession session;
	
	@Autowired
	UserRepository userrepo;	
	
	@Autowired
	Logger logger;
	
	@Autowired
	CheckUserActionInterface Checker;
	
	/**
	 * *Méthode pour afficher le panier d'un user
	 * @param model object Model pour passer un attribut à la vue
	 * @return String la page du panier
	 */
	@RequestMapping
	public String AfficherPanier(Model model) {
		if(!Checker.isValidUser(session))
			return "redirect:/welcome";

		User user = (User) session.getAttribute("user");
		List<Panier> list = (List<Panier>) panierservice.AfficherPanierbyUser(user);	
		if(list.size() == 0) {
			return "errEmptyCart";
		}
		model.addAttribute("panier", list);
		model.addAttribute("total", panierservice);
		return "panier";
	}

	/**Permet de supprimer un article du panier grâce à son id
	 * @param model object Model pour passer un attribut à la vue
	 * @param id du panier
	 * @param user du panier
	 * @return La vue du panier actualisé
	 */
	@RequestMapping(path = "/delete/{id}")
	public String deleteArticleById(Model model, 
			@PathVariable("id") Integer id,
			@SessionAttribute(name = "user", required = false) User user) {
		if(!Checker.isValidUser(session))
			return "redirect:/welcome";
		

		logger.info("article supprimé avec succès");
		panierservice.SupprimerPanier(id, user);

		return "redirect:/panier";
	}

	/**Méthode pour mettre à jour la quantité d'un article du panier
	 * @param Idarticle de l'article
	 * @param qte quantité à mettre à jour
	 * @param user du panier
	 * @return la vue update
	 */
	@RequestMapping(path = "/updateqte")
	public String updateParam(@RequestParam("idarticle") Integer Idarticle, 
			@RequestParam("qte") Integer qte, 
			@SessionAttribute(name = "user", required = false) User user) {

		if(!Checker.isValidUser(session))
			return "redirect:/welcome";
		
		logger.info("idarticle rest" +Idarticle);
		logger.info("qte rest" +qte);
		logger.info("quantité mis à jour");
		panierservice.UpdateQuantite(Idarticle, qte, user);

		return "redirect:/panier";
	}

	/**Ajoute un article au panier
	 * @param Idarticle de l'article
	 * @param qte quantité à ajouter
	 * @param user du panier
	 * @return la vue du panier
	 */
	@RequestMapping(path = "/add")
	public String AjouterAuPanier(@RequestParam(name="idarticle") Integer Idarticle, 
			@RequestParam(name="qte") Integer qte,
			@SessionAttribute(name = "user", required = false) User user) {

		//If user == null retour login
		if(!Checker.isValidUser(session))
			return "redirect:/welcome";

		//recuperer la session lié à l'user dans un objet user
		panierservice.AjouterAuPanier(Idarticle, user, qte);

		logger.info("Article ajouté au panier");

		return "redirect:/catalogue";
	}
}
