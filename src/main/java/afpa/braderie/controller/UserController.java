package afpa.braderie.controller;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import afpa.braderie.beans.User;
import afpa.braderie.service.UserService;

/**
 * Controlleur utilisé pour manipuler l'objet User à travers le modèle et la vue
 * Une seule méthode utilisée : validationLogin pour valider la connexion d'un utilisateur
 * @author 31010-83-06
 *
 */

@Controller
@SessionAttributes({"user"})
public class UserController {

	/*Injection du Logger*/
	@Autowired
	Logger logger;

	@Autowired 
	HttpSession session;


	@ModelAttribute("user")
	public User user() {
		return new User();
	}

	@Autowired
	private UserService userService;

	/**
	 * Méthode permettant de valider la connexion d'un User ou non
	 * @param model le modèle a retourner à la vue 
	 * @param login le login entré par l'utilisateur dans la vue de login
	 * @param pass le MDP entré par l'utilisateur dans la vue de login
	 * @return la vue du catalogue + l'user connecté à la session si la connexion est validé ou renvoie 
	 * à la page de login avec un message d'erreur
	 * 
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String validationLogin( Model model, 
			@RequestParam(name="username") String login, 
			@RequestParam(name="password") String pass) {
		//Si le test service est true, on envoie au catalogue + on associe le user
		if(userService.isLoginValid(login, pass)) {
			session.removeAttribute("error");
			boolean validToken = true;
			model.addAttribute("user", userService.getUser(login));
			session.setAttribute("user", userService.getUser(login));
			session.setAttribute("jeton", validToken);
			
			return "redirect:/catalogue";
			
		}else {
			session.removeAttribute("delog");
			session.setAttribute("error", "Erreur de connexion !");
			return "redirect:/welcomeError";
		}
	}

	/**
	 * Méthode itilisé pour déconnecter l'user, on supprime l'objet user rattaché à la session
	 * @param model le modèle avec l'attribut user en paramétre
	 * @return la vue sur la page de login et le user supprimé de la session
	 */
	@RequestMapping(value = "/logout")
	public String logout(Model model) {
		//On récupère le user de la session
		User userToDelog = (User) session.getAttribute("user");		
		logger.info("Deconnexion du user : " + userToDelog.getLogin());

		/* Suppression de la session*/
		if(session != null)
			session.invalidate();

		session.setAttribute("delog", "Deconnexion validée");
		return "redirect:/welcome";
	}


	/**
	 * Méthode utilisé pour afficher la page de login
	 * @return la vue page de login
	 */
	@RequestMapping(value = "/welcome")
	public String setWelcomePage() {
		session.removeAttribute("error");
		return "index";
	}

	/**
	 * Méthode utilisé pour afficher la page de login
	 * @return la vue page de login avec l'erreur
	 */

	@RequestMapping(value = "/welcomeError")
	public String setWelcomePageWError() {
		return "index";
	}
}
