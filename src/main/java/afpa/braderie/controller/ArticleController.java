package afpa.braderie.controller;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import afpa.braderie.service.ArticleService;
import afpa.braderie.service.CheckUserActionInterface;

/**
 * Controller de la classe article qui nous permet d'afficher la vue
 * 
 * @author ClémentF.
 *
 */
@Controller
public class ArticleController {
	
	@Autowired
	CheckUserActionInterface Checker;

	@Autowired
	Logger logger;
	
	@Autowired
	ArticleService articleService;
	
	@Autowired 
	HttpSession session;
	
	/**
	 * Va chercher la liste des articles et les renvoi à la vue
	 * ici "catalogue.html"
	 * 
	 * @param model pour passer les data du back à la vue
	 * @return la page à afficher
	 */
	@GetMapping("/catalogue")
	public String showCatalogue(Model model) {
		if(!Checker.isValidUser(session))
			return "redirect:/welcome";
		
		logger.info("Dans ArticleController.showCatalogue");
		model.addAttribute("articles", articleService.getAllArticle());
		return "catalogue"; 
	}
}
