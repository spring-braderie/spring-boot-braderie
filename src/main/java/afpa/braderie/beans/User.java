package afpa.braderie.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 31010-83-06
 *
 *POJO User correspond à un utilisateur de la braderie
 *
 */
@Entity
@Table(name = "t_user")
public class User {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer iduser;

	@Column(name = "login", unique = true)
	private String login;

	@Column(name = "pass")
	private String pass;


	@Column(name = "nbconnexion")
	private Integer nbconnexion;

	/**
	 * constructeur par défaut
	 */
	public User() {
	}
	
	/**
	 * @param iduser {@link User#getIduser}
	 * @param login  {@link User#getLogin}
	 */
	public User(Integer iduser, String login) {
		this.iduser = iduser;
		this.login = login;
	}

	public User(String login) {
		this.login = login;
	}

	/**
	 * Correspond a l'id de l'utilisateur dans la BDD
	 * il est utilisé pour retrouver les paniers de l'utilisateur
	 * @return the iduser
	 */
	public Integer getIduser() {
		return iduser;
	}

	/**
	 * {@link User#getIduser}
	 * @param iduser est id à modifier
	 */
	public void setIduser(Integer iduser) {
		this.iduser = iduser;
	}

	/**
	 * Correspond au nom utilisé pour se connecter au site
	 * permets aussi d'afficher le pseudo de l'utilisateur sur le site
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * {@link User#getLogin}
	 * @param login correspond au login a changer
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Mot de passe utilisé pour se connecter au site
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}

	/**
	 * {@link User#getPass}
	 * @param pass the pass to set
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}

	/**
	 * Compteur permettant de connaitre le nombre de fois où l'utilisateur s'est connecté au site
	 * @return the nbconnexion
	 */
	public Integer getNbconnexion() {
		return nbconnexion;
	}

	/**
	 * {@link User#getNbconnexion} 
	 * @param nbconnexion the nbconnexion to set
	 */
	public void setNbconnexion(Integer nbconnexion) {
		this.nbconnexion = nbconnexion;
	}

	@Override
	public String toString() {
		return "User [iduser=" + iduser + ", login=" + login + ", pass=" + pass + ", nbconnexion=" + nbconnexion + "]";
	}
}
