package afpa.braderie.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * POJO Article qui correspond à la table t_article de la BDD
 * Spring Data JPA
 * @author Clément
 *
 */

/*@Entity = Indique que notre classe est une entité de la BDD
//@Table = On défini le nom de la table dans la BDD*/
@Entity
@Table(name="t_article")
public class Article {
	
	/*Ces annonations permettent de préciser que cet attribut est un id et qu'il
	sera incrémenter automatique*/
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idarticle")
	private Integer id;
	
	@Column(name="description")
	private String description;
	
	@Column(name="marque")
	private String marque;
	
	@Column(name="prixunitaire")
	private Float prixunitaire;
	
	
	/*Constructeurs de Article qui permettent d'instancier des Articles*/
	public Article() {
	}
	
	public Article(Integer id, String description, String marque, Float prixunitaire) {
		this.id = id;
		this.description = description;
		this.marque = marque;
		this.prixunitaire = prixunitaire;
	}
	
	public Article(String description, String marque, Float prixunitaire) {
		this.description = description;
		this.marque = marque;
		this.prixunitaire = prixunitaire;
	}

	/*Guetteurs et Setteurs*/
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public Float getPrixunitaire() {
		return prixunitaire;
	}

	public void setPrixunitaire(Float prixunitaire) {
		this.prixunitaire = prixunitaire;
	}

	/*Redéfinition de la méthode toString d'Object*/
	@Override
	public String toString() {
		return "Article [id=" + id + ", description=" + description + ", marque=" + marque + ", prixunitaire="
				+ prixunitaire + "]";
	}
}
