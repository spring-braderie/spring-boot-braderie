package afpa.braderie.beans;

import java.beans.Transient;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**POJO panier
 * @author jordy
 *
 */
@Entity
@Table(name = "t_panier")
public class Panier implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idpanier", unique = true, nullable = false)
	private Integer idpanier;

	@OneToOne
	@JoinColumn(name = "iduser")
	private User user;

	@OneToOne
	@JoinColumn(name = "idarticle")
	private Article article;

	@Column(name = "quantite")
	private Integer quantite;

	//Les constructeurs
	public Panier() {

	}

	public Panier(User user, Article article, Integer quantite) {
		this.user = user;
		this.article = article;
		this.quantite = quantite;
	}

	public Panier(Integer idpanier, User user, Article article, Integer quantite) {
		this.idpanier = idpanier;
		this.user = user;
		this.article = article;
		this.quantite = quantite;
	}


	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("{\"Panier\" :");
		builder.append("{\"idpanier\" :");
		builder.append(idpanier);
		builder.append(", \"user\" :");
		builder.append(user.getLogin());
		builder.append(", \"article\" :");
		builder.append(article);
		//  builder.append(article);
		builder.append(", \"quantite\" :");
		builder.append(quantite);
		builder.append("}");
		builder.append("}");
		return builder.toString();
	}

	/**
	 * @return the idpanier
	 */
	public Integer getIdpanier() {
		return idpanier;
	}

	/**
	 * @param idpanier the idpanier to set
	 */
	public void setIdpanier(Integer idpanier) {
		this.idpanier = idpanier;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the article
	 */
	public Article getArticle() {
		return article;
	}

	/**
	 * @param article the article to set
	 */
	public void setArticle(Article article) {
		this.article = article;
	}

	/**
	 * @return the quantite
	 */
	public Integer getQuantite() {
		return quantite;
	}

	/**
	 * @param quantite the quantite to set
	 */
	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}

	public void increaseQuantity() {
		this.quantite++;
	}

	/**Permet de calculer le soustotal du panier
	 * @return float egal au sous-total du panier
	 */
	@Transient
	public float getSousTotal() {
		return this.article.getPrixunitaire() * quantite;

	}
}


