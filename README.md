Ajout de la procédure stocké isValidLogin() dans le scriptSQL
- 1- Clique droit sur la base de données braderie -> Query Tool
- 2- Copier coller ce script : 
    
```
CREATE OR REPLACE FUNCTION isValidLogin(IN username varchar, IN password varchar)
    RETURNS boolean
    AS $$
    BEGIN
    IF EXISTS(SELECT * FROM t_user WHERE login = username AND pass = password) THEN
    RETURN 'true';
    ELSE
    RETURN 'false';
    END IF;
    END;
    $$language plpgsql;
```

Script de la base de données utilisée:

```
/******************************************************************************* 
                                SUPPRESSION DES TABLES
*******************************************************************************/

DROP TABLE IF EXISTS T_ARTICLE CASCADE;
DROP TABLE IF EXISTS T_USER CASCADE;
DROP TABLE IF EXISTS T_PANIER CASCADE;

/******************************************************************************* 
                                CREATION DES TABLES
*******************************************************************************/
CREATE TABLE T_ARTICLE
(	IDARTICLE SERIAL NOT NULL, 
	DESCRIPTION VARCHAR(25) NOT NULL, 
	MARQUE VARCHAR(25) NOT NULL, 
	PRIXUNITAIRE NUMERIC(5,2) NOT NULL, 
	 CONSTRAINT "PK_CT_TARTICLE" PRIMARY KEY (IDARTICLE)
);

CREATE TABLE T_USER
(	IDUSER SERIAL NOT NULL, 
	LOGIN VARCHAR(25) NOT NULL, 
    PASS VARCHAR(25) NOT NULL,
    NBCONNEXION NUMERIC(12) DEFAULT 0, 
	 CONSTRAINT "PK_TUSER" PRIMARY KEY (IDUSER)
);

CREATE TABLE T_PANIER
(   IDPANIER SERIAL NOT NULL,
    IDUSER SERIAL NOT NULL,
    IDARTICLE SERIAL NOT NULL,
    QUANTITE NUMERIC(4) NOT NULL,
    CONSTRAINT "PK_TPANIER" PRIMARY KEY (IDPANIER),
    CONSTRAINT "FK_IDUSER" FOREIGN KEY(IDUSER) REFERENCES T_USER (IDUSER),
	CONSTRAINT "FK_IDARTICLE" FOREIGN KEY(IDARTICLE) REFERENCES T_ARTICLE (IDARTICLE)
);

/******************************************************************************* 
                                PROCEDURE STOCKEE
*******************************************************************************/
CREATE OR REPLACE FUNCTION isValidLogin(IN username varchar, IN password varchar)
RETURNS boolean
AS $$
BEGIN
  IF EXISTS(SELECT * FROM t_user WHERE login = username AND pass = password) THEN
    RETURN 'true';
  ELSE
    RETURN 'false';
  END IF;
END;
$$language plpgsql;


/******************************************************************************* 
                               INSERTION DES DONNEES
*******************************************************************************/ 
  
INSERT INTO T_USER (LOGIN,PASS,NBCONNEXION) VALUES ('Marleyb', 'marleyb123', 0);
INSERT INTO T_USER (LOGIN,PASS,NBCONNEXION) VALUES ('Charliep', 'charliep123', 0);
INSERT INTO T_USER (LOGIN,PASS,NBCONNEXION) VALUES ('Milesd', 'milesd123', 0);
INSERT INTO T_USER (LOGIN,PASS,NBCONNEXION) VALUES ('Keithj', 'keithj123', 0);
    
INSERT INTO T_ARTICLE (DESCRIPTION, MARQUE, PRIXUNITAIRE) VALUES ('Bonbon', 'Haribo', 0.2);
INSERT INTO T_ARTICLE (DESCRIPTION, MARQUE, PRIXUNITAIRE) VALUES ('Stylo Bleu', 'Bic', 1);
INSERT INTO T_ARTICLE (DESCRIPTION, MARQUE, PRIXUNITAIRE) VALUES ('Ramette 80 grs', 'ClaireFontaine', 5);
INSERT INTO T_ARTICLE (DESCRIPTION, MARQUE, PRIXUNITAIRE) VALUES ('Equerre', 'Castorama', 2);
INSERT INTO T_ARTICLE (DESCRIPTION, MARQUE, PRIXUNITAIRE) VALUES ('Montre', 'Rolex', 200);
INSERT INTO T_ARTICLE (DESCRIPTION, MARQUE, PRIXUNITAIRE) VALUES ('Velo', 'Decathlon', 190);
INSERT INTO T_ARTICLE (DESCRIPTION, MARQUE, PRIXUNITAIRE) VALUES ('Rameur', 'Decathlon', 290);
```

Commandes pour l'application :

- install :  mvnw install clean
- test    :  mvnw test
- launch  :  mvnw spring-boot:run
- javadoc :  mvnw javadoc:javadoc

Adresse de l'application dans le navigateur : 

- localhost:8080/SpringBraderie-CFJBLGCV

